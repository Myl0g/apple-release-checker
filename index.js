#! /usr/bin/env node

const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const fetch = require("node-fetch");

const supported = String(process.env.RELEASE_CHECKER_OSES).toUpperCase().split(" ");

function log(text) {
    if (process.env.RELEASE_CHECKER_DEBUG_MODE) {
        console.log(text);
    }
}

function checkForUpdate(releaseTitle) {
    const arr = String(releaseTitle).split(" ");
    const osUpper = arr[0].toUpperCase();
    if (!supported.includes(osUpper)) {
        log("- " + arr[0] + " not supported. Not checking.");
        return false;
    }

    let osVersion;
    if (arr[0] === "macOS") { // e.g. macOS Catalina 10.15 beta 2 (19A487l)
        osVersion = arr[2];
    } else { // e.g. iOS 12.4 beta 5
        osVersion = arr[1];
    }
    log("- Determined that OS version of release is " + osVersion);

    const beta = parseInt(arr[arr.indexOf("beta") + 1]); // e.g. 2
    log("- Determined beta of this release to be " + beta);

    const { envVar, versionEnvVar } = { "envVar": osUpper + "_CURRENT_BETA", "versionEnvVar": osUpper + "_CURRENT_VERSION" };
    log("- Using env var " + envVar);
    log("- Value of env var = " + process.env[envVar]);

    return process.env[envVar] && beta > parseInt(process.env[envVar]) && process.env[versionEnvVar] && osVersion === process.env[versionEnvVar];
}

function textToAppleReleases(text) {
    const html = new JSDOM(text).window.document;
    log("Parsed HTML.");

    const releases = html.getElementById("main").getElementsByClassName("article-title");
    log("Got releases.");

    return releases;
}

function loopOverReleases(releases) {
    const alreadyNotified = [];

    for (let item of releases) {
        const releaseTitle = item.firstChild.textContent;
        if (alreadyNotified.includes(releaseTitle.split(" ")[0])) {
            log("Already notified user about " + releaseTitle);
            continue;
        }
        log("Retrieving " + releaseTitle);

        if (checkForUpdate(releaseTitle)) {
            console.log("A new beta release is available: " + releaseTitle);
            alreadyNotified.push(releaseTitle.split(" ")[0]);
        }
    }
}

supported.forEach((supportedOS) => {
    [supportedOS + "_CURRENT_BETA", supportedOS + "_CURRENT_VERSION"].forEach((envVar) => {
        if (!process.env[envVar]) {
            log("Environment variable " + envVar + " not found. Please set and re-run.");
            process.exit(1);
        }
    });
});

if (!process.env.RELEASE_CHECKER_TESTING_MODE) {
    fetch("https://developer.apple.com/news/releases/").then((val) => {
        log("Fetched releases URL.");
        val.text().then((txt) => {
            log("Parsed text.");
            loopOverReleases(textToAppleReleases(txt));
        });
    });    
} else {
    loopOverReleases(textToAppleReleases(require("fs").readFileSync("test.html")));
}
